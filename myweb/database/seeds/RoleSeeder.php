<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data1 = Role::create([
        	'role_name' => 'Admin'
        ]);

        $data1->save();

        $data1 = Role::create([
        	'role_name' => 'User'
        ]);

        $data1->save();
    }
}
