<?php

use Illuminate\Database\Seeder;
use App\Campaign;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data1 = Campaign::create([
        	'id'	=> '92d24043-7d83-4b42-bbf2-266c462dba9f',
        	'title' => '#BisaSembuh - Donasi untuk Biaya Pengobatan',
        	'description' => "Setiap harinya ada ratusan orang harus menggalang dana demi membayar biaya pengobatan, operasi, dan kebutuhan penunjang medis lainnya. Tergerak untuk mengajak ribuan #OrangBaik untuk membantu saudara-saudara kita yang sedang berjuang untuk sembuh, GmiDonasi membuat program #BisaSembuh. Kita ingin mengajakmu berdonasi membantu saudara-saudara kita yang sedang berjuang untuk sembuh. Donasimu akan sangat berarti bagi saudara-saudara kita yang sedang berjuang untuk sembuh. ",
        	'image' => '/photos/campaign/92d24043-7d83-4b42-bbf2-266c462dba9f.webp',
            'address' => 'Jalan Kalimantan no 8',
            'required' => 27000000,
            'collected' => 26599874
        ]);

        $data1->save();

        $data2 = Campaign::create([
        	'id' => 'afba02da-aabf-437f-b9b9-4ef6617bdde4',
        	'title' => 'Bantu Pasien Rumah Sakit #BisaBerobat',
        	'description' => "Ada banyak saudara kita yang  sedang berjuang di rumah sakit untuk sembuh namun butuh bantuan biaya pengobatan. Tergerak dari situasi tersebut, GmiDonasi membuat halaman galang dana ini. Khusus untuk menyalurkan bantuan biaya pengobatan pasien-pasien berdasarkan rekomendasi dokter dan pihak Rumah Sakit langsung. ",
        	'image' => '/photos/campaign/afba02da-aabf-437f-b9b9-4ef6617bdde4.webp',
            'address' => 'Jalan Jawa no 8',
            'required' => 15000000,
            'collected' => 13191977
        ]);

        $data2->save();

        $data3 = Campaign::create([
        	'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e62',
            'title' => 'Tetap Cari Nafkah Meski Tumor Di Rahangnya Kritis',
        	'description' => "Sore itu hujan turun, hawa pun berubah dingin. Terdengar teriakan Pak Arifin, tahan sakit tumor di rahangnya yang terus membesar. Tiap dingin menyerang, sakitnya pun bertambah 10x lipat! Belum lagi sakitnya bertambah parah, seiring darah sering keluar dari rahangnya yang diserang tumor itu. Ia hanya bisa menangis sakit saat tangan sang istri selalu sigap lap cairan darah itu. Namun, rasa sakit ini bukan alasan Pak Arifin berhenti nafkahi keluarga. Sosoknya tiap hari terlihat, di tempat cukur rambut. Kesigapan tangannya, rambut pelanggan terbentuk indah. Meski kadang ia sering izin pulang lebih dulu, jika tubuhnya terlalu sakit karena tumor itu sangat berat ia bawa. Namun esok harinya, ia kembali datang, berharap setiap rambut yang terjatuh isi dompetnya untuk nafkahi anak istri. Upahnya yang hanya 600 ribu/bulan, sedikit ia sisihkan untuk bisa berobat ke puskesmas. Meski dokter bilang, tumornya harus segera dioperasi jika tak ingin menyebar ke tubuhnya yang lain. ",
        	'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e62.webp',
            'address' => 'Jalan Gagak no 8',
            'required' => 102097955,
            'collected' => 198050000
        ]);

        $data3->save();

        $data4 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e04',
            'title' => 'Sembuhkan Penghafal Quran dari Tulang Bengkok',
            'description' => "Santri Pesantren Tahfiz Darul Hijrah, Tamanduyu, Pasuruan itu kini tinggal menyelesaikan 9 juz Al-Qur'an lagi untuk bisa lulus menjadi seorang Hafizh. Padahal penyakitnya itu membuat Anang merasakan nyeri yang menjalar dari tulang belakang ke kaki, punggung hingga pinggul. Ia kerap kali merasaka kesakitan ketika harus berjalan ataupun berdiri. Setelah diperiksa Dokter, Anang harus secepatnya diberi tindakan. Penyakit tulang bengkok yang dibiarkan akan membahayakan tubuh. Bagi Ibu Anang, sulit sekali mengumpulkan dana yang cukup besar untuk pengobatan buah hatinya. Sang Ayah yang sudah meninggal dunia sejak Anang kecil membuatnya ibunya harus berjuang seorang diri. Bersama lancarkan perjuangan Anang dengan bantu sembuhkan ia dari penyakit Tulang Bengkok.",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e04.webp',
            'address' => 'Jalan Gagak no 8',
            'required' => 200000000,
            'collected' => 102922256
        ]);

        $data4->save();

        $data5 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e05',
            'title' => 'Gagal Ginjal, Bantu Roni Sambung Hidupnya',
            'description' => "“Saya cuman kepengen sembuh, berbakti lagi sama orang tua, gak begini nyusahin orang tua. Saya selalu berdoa agar diberi kesembuhan dan umur yang panjang. Saya gak bisa bayangin bagaimana nasib kedua orang tua saya ketika saya dipanggil Yang Maha Kuasa...” ujar Roni dengan mata berkaca-kaca. Sudah setahun lebih Roni mengidap penyakit gagal ginjal, badannya lemas dan kurus, nafasnya sering sesak. Roni pun terpaksa berhenti bekerja. Untuk menjaga kondisinya tetap stabil, Roni harus cuci darah 2-3x seminggu dengan perjalanan1 jam menuju Rumah Sakit Umum di Situbondo. Karena jarak rumah sakit yang jauh, seringkali Roni melewatkan jadwal cuci darahnya. Beberapa obat khusus pun juga tak bisa Roni tebus karena terkendala biaya. Kondisi Roni saat ini semakin memburuk, ginjalnya telah mempengaruhi organ tubuh yang lain. Untuk bisa cepat sembuh, Roni harus segera mendapatkan perawatan yang optimal. #OrangBaik, kini kamu sudah dengar bagaimana perjuangan Roni dan keluarga. Sudah saatnya kita ulurkan tangan dan jangan biarkan mereka berjuang sendiri. ",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e05.webp',
            'address' => 'Jalan Gagak no 8',
            'required' => 137635200,
            'collected' => 26851870
        ]);

        $data5->save();

        $data6 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e06',
            'title' => 'Zakat Untuk Ribuan Penghafal & Pecinta Al Quran',
            'description' => "Penyaluran zakat berjangkauan luas demi membangun pendidikan, sosial dan kemanusiaan kepada masyarakat yang berhak menerimanya melalui program Zakat untuk Ribuan Penghafal dan Pecinta Al Quran Nusantara. RQV Indonesia sudah menyentuh 10,547 penerima manfaat. Jangkauannya sudah tersebar di 11 Provinsi di Indonesia. Para Penghafal dan Pecinta Al Qur’an yang berasal dari berbagai daerah. Anak-anak penghafal Al Qur’an yang belajar baca tulis dan hafal Al Qur’an serta anak-anak muda yang dilatih di sekolah Tahfidzul Qur’an Berkarakter untuk menjadi Duta Qur’an yang bisa berkontribusi di lapangan mengajarkan Qur’an kepada masyarakat pelosok negeri. Mari bantu dan berikan kesempatan kepada anak-anak Indonesia yang berjuang bersama Al Qu’ran agar dapat terus bersekolah lewat Zakat yang kita berikan.",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e06.webp',
            'address' => 'Baleendah, Bandung',
            'required' => 750000000,
            'collected' => 580140796
        ]);

        $data6->save();

        $data7 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e07',
            'title' => '#BisaSembuh - Donasi untuk Para Pejuang Kesembuhan',
            'description' => "Setiap harinya ada ratusan pasien yang menggalang dana demi membayar biaya pengobatan, operasi, dan kebutuhan penunjang medis lainnya. Karenanya, GmiDonasi tergerak untuk mengajak ribuan #OrangBaik untuk membantu saudara-saudara kita yang sedang berjuang untuk sembuh melalui program #BisaSembuHospital. Kita ingin mengajakmu berdonasi membantu saudara-saudara kita yang sedang berjuang untuk sembuh. Donasimu akan sangat berarti bagi saudara-saudara kita yang sedang berjuang untuk sembuh. ",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e07.webp',
            'address' => 'Trenggalek, Jawa Timur',
            'required' => 2000000000,
            'collected' => 1796994681
        ]);

        $data7->save();

        $data8 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e08',
            'title' => 'KANKER STADIUM 4! Nyawa Sutarman Di Ujung Tanduk',
            'description' => "Sesekali Pak Sutarman mengusap bagian yang menonjol di leher kanannya, sambil menahan sakit ia pun tak jarang melihat foto dirinya saat 2 tahun kebelakang dimana dirinya masih terlihat sehat dan berisi. Rasa sakit di gusi yang pria berusia 36 tahun ini alami pada tahun 2018 lalu, kini berubah menjadi ‘Monster’ yang terus mengancam nyawanya. Ca Sinonasal, adalah kanker ganas yang menyerang ayah 2 orang anak ini. Saat divonis kanker stadium 4 membuat Pak Sutarman sangat terpukul dan kaget luar biasa. Yang menjadi beban pikirannya adalah istri dan kedua anaknya yang harus dinafkahi. Untuk bertahan hidup, sang istri, Ibu Mila (25) bahkan rela bekerja mengurus kambing-kambing milik tetangga, dengan upah yang jauh dari kata cukup. Ia harus pintar-pintar dalam menggunakan upah yang ia dapat untuk suami yang sakit dan kedua anaknya. Biaya kemoterapi dan transportasi yang tak murah menjadi kendala Pak Sutarman untuk melakukan perawatan demi kesembuhannya. Padahal jika terus dibiarkan, sel kanker akan terus mengganas dan bahkan mengancam nyawanya. Oleh karenanya, Pak Sutarman membutuhkan uluran tangan para #orangbaik agar bisa sembuh, agar kedua anaknya tidak menjadi seorang anak yatim di usia sangat dini.",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e08.webp',
            'address' => 'Tegalega, Bandung',
            'required' => 198050000,
            'collected' => 80353855
        ]);

        $data8->save();

        $data9 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e09',
            'title' => 'Bantu Rina Pulih dari TB Tulang Belakang',
            'description' => "Tahun ini Rina (23th) baru saja menyelesaikan studinya di perguruan tinggi. Kerja keras yang ia tuangkan dalam lembaran skripsi berisi harapan kelak berdiri berdampingan bersama orang tua memakai toga impian. Namun sayang, mimpi tersebut terhalang oleh TB Tulang. Setahun lalu, Rina selalu mengeluh sakit pada punggungnya. Dari Ramadhan lalu, ia bolak-balik ke RS untuk diperiksa, namun Dokter belum mendeteksi penyakitnya. Kondisi penyakitnya mengkhawatirkan, ruas tulangnya retak serta berisi nanah sehingga menonjol keluar. Bahkan parahnya, ada saraf yang putus sehingga Rina harus segera operasi. Jika tidak, tulang lehernya rapuh dan kepalanya tak punya penyangga! Kedua orangtuanya bekerja sebagai Asisten Rumah Tangga (ART) dengan upah tak besar. Tinggal menumpang dirumah majikan Ayah, Rina hanya bisa terkapar lemah. #OrangBaik, Semangat dan mimpi Rina memakai Toga di hari wisudanya masih tersimpan dalam hati. Meski kini kondisinya sakit, ia berdoa untuk bisa segera sembuh. ",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e09.webp',
            'address' => 'Soreang, Kabupaten Bandung',
            'required' => 200000000,
            'collected' => 163264813
        ]);

        $data9->save();

        $data10 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e10',
            'title' => 'Kanker Renggut Mata & Langkahnya, Ia Makin Kritis!',
            'description' => "Kanker Terus Sebarkan Rasa Sakit Dari Mata, Kepala Hingga Punggungnya. Ia Tinggal Sebatang Kara, Fakhrudin Tak Bisa Berobat. Seluruh pandangannya gelap, terasa panas dan perih. Fakhrudin berusaha bangun, meraba dinding dan berjalan. Langkah lemahnya hampir jatuh, namun lengan kakak menolongnya. Kesendiriannya dalam hidup ternyata berakhir pilu. Kanker renggut dua matanya yang terus membengkak, hingga kini ia tak bisa melihat. Sakit, ia rasakan dari punggung hingga kepala. Sang kakak, tiap hari berusaha rawat adiknya, beri makan dan papahnya berjalan. Kondisi ekonomi yang kian sulit, buat jadwal kemo Fakhrudin terlewat karena belum ada biaya. #Sahabat, kini kondisi Fakhrudin makin kritis dan harus segera jalani operasi untuk bertahan! Uluran tangan baikmu bisa bantu Fakhrudin bertahan.",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e10.webp',
            'address' => 'Pejaten, Jakarta Selatan',
            'required' => 130000000,
            'collected' => 45043697
        ]);

        $data10->save();

        $data11 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e11',
            'title' => 'Suami Wafat, Ibu 2 Anak Kini Berjuang Lawan Kanker',
            'description' => "Semenjak suaminya meninggal dunia 6 tahun lalu, Bu Noneng banting tulang bekerja sebagai Asisten Rumah Tangga untuk menghidupi dan membiayai sekolah kedua anaknya. Kini Bu Noneng menderita Kanker Payudara Stadium 3B. Bu Noneng memendam penyakitnya karena tidak mau anak-anaknya khawatir. Ketika akhirnya memeriksakan diri ke puskemas, ternyata kanker di payudaranya sudah stadium 3B. Bu Noneng memikirkan masa depan kedua anaknya karena ia mereka menggantungkan hidup pada Bu Noneng. Satu anaknya masih duduk di bangku SMA dan satu lagi sudah lulus kuliah. #OrangBaik, Anak-anak Bu Noneng berharap ibu mereka satu-satunya bisa segera pulih.",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e11.webp',
            'address' => 'Serang, Banten',
            'required' => 167670000,
            'collected' => 123960117
        ]);

        $data11->save();

        $data12 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e12',
            'title' => 'Cangkok Hati untuk Rika dan Bayinya',
            'description' => "Rika (35 tahun) hanya bisa melihat bayinya yang baru lahir lewat video di handphonenya. Pengerasan hati, pendarahan esofagus, serta komplikasi akibat serangan virus memaksa Rika untuk berpisah dari bayinya tepat setelah ia melahirkan. Tidak hanya terpisah dari anaknya, Rika juga harus berpisah dari keluarga yang selalu memberinya semangat dan cinta, serta tak lagi bisa menolong hewan-hewan yang terlantar tanpa rumah. Inilah cerita perjuangan Rika untuk meraih kembali segala anugerah yang telah diberikan Tuhan kepadanya. Dari sana, badai menerjang dan membalikkan nasib hidupnya. Berkali-kali Rika mengalami pendarahan saat kehamilannya, tubuhnya kian melemah, dan kemudian Rika harus melahirkan anaknya secara Sesar. Meski demikian, anaknya lahir dengan selamat. Chloe miracle baby, begitu mereka menyebutnya. Seorang bayi kuat yang berhasil lahir dari kemustahilan. Kini, Chloe harus lanjut berjuang di NICU. Yuk bersama kita bantu kesembuhan Rika lewat operasi cangkok hati. ",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e12.webp',
            'address' => 'Jakarta Pusat',
            'required' => 2100000000,
            'collected' => 1209101332
        ]);

        $data12->save();

        $data13 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e13',
            'title' => 'Tangannya Tak Lagi Bisa Bantu Sesama Karena Tumor',
            'description' => "Akmal (20) tidak pernah bisa melihat orang lain dalam kesulitan. Itulah yang menjadi penggerak hatinya untuk aktif menjadi relawan di segala kesempatan yang ia bisa. Termasuk melihat kakak yang bekerja sendirian di laut karena menggantikan bapak yang telah pensiun. Namun, itu adalah awal dari tumor mulai tumbuh, 6 bulan lalu. Semula Akmal terkilir saat membantu sang kakak, mengangkat beban ikan pari besar. Selang 2 minggu, belum rasa nyeri itu hilang, ia mengalami kecelakaan yang membuatnya terjatuh dari sepeda motor. Kendala biaya menjadi penyebab Akmal hanya dapat diobati dengan daun herbal yang telah dikumpulkan ibu, untuk mengompres tumor yang kian hari kian membesar dan memerah. Sahabat, belum sekalipun tangan Akmal mendapatkan tindakan operasi, yang sangat ia butuhkan agar tumor tidak berkembang lebih luas.",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e13.webp',
            'address' => 'Coblong, Bandung',
            'required' => 119995000,
            'collected' => 29101534
        ]);

        $data13->save();

        $data14 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e14',
            'title' => 'MARI KITA BANTU HAPUS AIR MATA ANAK YATIM PIATU',
            'description' => "Anggara (11Th), Dwi (7Th), dan Asih (4Th) mereka 3 bersaudara yang ditinggal meninggal sama kedua orangtuanya. Di usia mereka yang masih belia, mereka sudah kehilangan kasih sayang kedua orang tua mereka. Ayahnya yang bekerja di Lampung meninggal akibat bencana tsunami Selat Sunda 2018 dan Ibunya meninggal karena sakit parah November 2019. Harta dan rumah mereka sudah terjual dan habis untuk biaya pengobatan rumah sakit ibunya. Mereka bertiga sudah tidak punya apa-apa lagi dan tidak tahu mau kemana. Alhamdulillah ada ustadz Rifa'i selaku pengurus yayasan yatim al-amanah nusantara yang langsung bergerak mengasuh mereka bertiga. Tidak mudah untuk mengembalikan semangat dan keceriaan mereka kembali. Ayo kita bantu hapus air mata anak yatim piatu dengan memberikan sedikit harta kita untuk makan, sekolah dan keperluan hidup lainnya. Ada sekitar 37 anak yatim piatu serta 44 anak dhuafa yang di asuh langsung ustadz Rifa'i.",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e14.webp',
            'address' => 'Yayasan Al-Amanah Nusantara',
            'required' => 1000000000,
            'collected' => 181777679
        ]);

        $data14->save();

        $data15 = Campaign::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e15',
            'title' => 'Lahir Tanpa Anus, Bantu Pengobatan Habib',
            'description' => "Habib adalah anak seorang petani yang lahir tanpa anus dan kelaminnya terbelah. Kini Adek Habib berusia 2 tahun dan bertahan setiap harinya dengan anus buatan di perut sebelah kirinya. Agar Habib bisa BAB, Ibu dan Bapaknya (Pak Rendi) harus mengganti kantong BAB (colostomy) Habib setiap hari,  dengan biaya 1.135.000/bulan. Habib pun harus segera operasi anus dan kelamin ke Jakarta, dengan biaya yang entah dari mana. Ditambah lagi, kebutuhan perban dan popoknya yang harus diganti 6 kali/hari. Padahal, penghasilan Pak Rendi sebagai petani paling banyak hanya Rp 80 ribu/hari. Pak Rendi banting tulang mencari pekerjaan tambahan sebagai buruh serabutan, namun pekerjaan tidak menentu dan ia harus membagi waktu dengan istrinya untuk bergantian membersihkan anus buatan Habib dan mengganti perbannya. “Saya berharap ada donatur baik yang ingin membantu Habib untuk memenuhi kebutuhan utamanya berupa kantong Colostomy (kantong BAB)” ujar Ayah habib. ",
            'image' => '/photos/campaign/d3aa7aa4-dc3d-4bc0-bada-0976afb31e15.webp',
            'address' => 'Buah Batu, Kota Bandung',
            'required' => 107104680,
            'collected' => 42441647
        ]);

        $data15->save();
    }
}
