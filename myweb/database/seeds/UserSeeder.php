<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$admin = Role::where('role_name','=','Admin')->first();
    	$user =	Role::where('role_name','=','User')->first();
        $data1 = User::create([
        	'name' => 'Gardhani',
        	'email'	=> 'gardhani@test',
        	'role_id' => $admin->id,
        	'email_verified_at' => now(),
        	'password' => Hash::make('12345678')
        ]);

        $data1->save();
        $data1->update([
            'photo' => "images/profile/jerry.jpeg"
        ]);

        $data2 = User::create([
        	'name' => 'Issaac',
        	'email'	=> 'issaac@test',
        	'role_id' => $user->id,
        	'password' => Hash::make('12345678')
        ]);

        $data2->save();
        $data2->update([
            'photo' => "images/profile/mouse2.jpeg"
        ]);
        $data3 = User::create([
            'name' => 'Daffa',
            'email' => 'daffa@test',
            'role_id' => $user->id,
            'email_verified_at' => now(),
            'password' => Hash::make('12345678')
        ]);

        $data3->save();
        $data3->update([
            'photo' => "images/profile/mouse3.jpeg"
        ]);
    }
}
