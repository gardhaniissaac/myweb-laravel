<?php

use Illuminate\Database\Seeder;
use App\Blog;

class BlogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data1 = Blog::create([
            'id'    => '92d24043-7d83-4b42-bbf2-266c462dba9f',
        	'title' => 'Berkah Bersedekah',
            'author' => 'Gardhani',
        	'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/92d24043-7d83-4b42-bbf2-266c462dba9f.png'
        ]);

        $data1->save();

        $data2 = Blog::create([
            'id' => 'afba02da-aabf-437f-b9b9-4ef6617bdde4',
        	'title' => 'Kewajiban Berzakat',
            'author' => 'Muhammad',
        	'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/afba02da-aabf-437f-b9b9-4ef6617bdde4.jpeg'
        ]);

        $data2->save();

        $data3 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e62',
        	'title' => 'Infaq, Sedekah, dan Zakat',
            'author' => 'Issaac',
        	'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e62.jpg'
        ]);

        $data3->save();

        $data4 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e04',
            'title' => 'Rezeki yang Berkah',
            'author' => 'Joko',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e04.png'
        ]);

        $data4->save();

        $data5 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e05',
            'title' => 'Bersedekah di Bulan Ramadhan',
            'author' => 'Ahmad',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e05.png'
        ]);

        $data5->save();

        $data6 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e06',
            'title' => 'Wajib Berzakat',
            'author' => 'Hamzah',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e06.png'
        ]);

        $data6->save();

        $data7 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e07',
            'title' => 'Membersihkan Harta dengan Bersedekah',
            'author' => 'Rizki',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e07.png'
        ]);

        $data7->save();

        $data8 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e08',
            'title' => 'Istiqomah dalam Bersedekah',
            'author' => 'Yusuf',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e08.png'
        ]);

        $data8->save();

        $data9 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e09',
            'title' => 'Zakat Mal',
            'author' => 'Ali',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e09.png'
        ]);

        $data9->save();

        $data10 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e10',
            'title' => 'Berbagai Macam Zakat',
            'author' => 'Ari',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e10.png'
        ]);

        $data10->save();

        $data11 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e11',
            'title' => 'Bersama Memuliakan Masjid',
            'author' => 'Tono',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e11.png'
        ]);

        $data11->save();

        $data12 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e12',
            'title' => 'Berbagi Bersama',
            'author' => 'Ridho',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e12.png'
        ]);

        $data12->save();

        $data13 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e13',
            'title' => 'Manfaat Selalu Menjaga Wudhu',
            'author' => 'Issaac',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e13.png'
        ]);

        $data13->save();

        $data14 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e14',
            'title' => 'Bersholawat Setiap Waktu',
            'author' => 'Issaac',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e14.png'
        ]);

        $data14->save();

        $data15 = Blog::create([
            'id' => 'd3aa7aa4-dc3d-4bc0-bada-0976afb31e15',
            'title' => 'Keberkahan diatas Kuantitas',
            'author' => 'Issaac',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'image' => '/photos/blog/d3aa7aa4-dc3d-4bc0-bada-0976afb31e15.png'
        ]);

        $data15->save();
    }
}
