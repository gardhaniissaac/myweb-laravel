<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>GmiDonasiApp</title>
	<link rel="stylesheet" type="text/css" href="{{url('/css/app.css')}}">
</head>
<body>
	<div id="app">
		<app></app>
	</div>

	<script src="{{url('/js/app.js')}}"></script>

</body>
</html>