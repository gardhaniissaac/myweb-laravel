<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap/dist/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.css" />
</head>
<body>
	<div id="verif">
		<v-app>
		  	<v-app-bar app color="#6495ED" dark>
		    	<v-toolbar-title><a href="/" style="text-decoration: none; color:white">GmiDonasiApp</a></v-toolbar-title>
		  	</v-app-bar>

		  	<v-card>
		        <v-toolbar dark color="#6495ED">
		            <v-btn icon dark @click.native="close">
		                <v-icon>mdi-close</v-icon>
		            </v-btn>
		            <v-toolbar-title>Verify Email</v-toolbar-title>
		        </v-toolbar>

		        <v-divider></v-divider>

		        <v-container fluid>
		        	<h3>
		        		Verify Email
		        	</h3>
		            <v-form ref="form" v-model="valid" lazy-validation>
		                <v-text-field
		                    v-model="otp"
		                    label="OTP Code"
		                    :rules="otpRules"
		                    :disabled = "verified"
		                    required
		                    append-icon="mdi-account-box"
		                >
		                </v-text-field>

		                <div class="text-xs-center">
		                    <v-btn
		                        color="success lighten-1"
		                        :disabled="!valid"
		                        @click="submit"
		                    >
		                        Submit
		                        <v-icon right dark>mdi-lock-open</v-icon>
		                    </v-btn>
		                </div>
		            </v-form>

		            <v-btn
		            	class="my-3"
		            	color="primary"
		            	@click="expired = !expired"
		            >
		            	Regenerate OTP
		        	</v-btn>

		        	<v-form ref="newpass" v-show="verified" v-model="verify" lazy-validation>
		            	<h3>Create New Password</h3>
		                <v-text-field
		                    v-model="emailVerified"
		                    :rules="emailRules"
		                    label="E-mail"
		                    disabled
		                    required
		                    append-icon="mdi-email"
		                >
		                </v-text-field>

		                <v-text-field
		                    v-model="password"
		                    :rules="passwordRules"
		                    label="Password"
		                    type= "password"
		                    required
		                    append-icon="mdi-eye-off"
		                    hint="At least 6 characters"
		                >
		                </v-text-field>

		                <v-text-field
		                    v-model="confirmPassword"
		                    :rules="passwordRules"
		                    label="Confirm Password"
		                    type= "password"
		                    required
		                    append-icon="mdi-eye-off"
		                >
		                </v-text-field>

		                <div class="text-xs-center">
		                    <v-btn
		                        color="success lighten-1"
		                        :disabled="!verify"
		                        @click="createPassword"
		                    >
		                        Create Password
		                        <v-icon right dark>mdi-lock-open</v-icon>
		                    </v-btn>
		                </div>
		            </v-form>            

		            <v-form ref="regenform" v-show="expired" v-model="expire" lazy-validation>
		            	<h3>Regenerate OTP</h3>
		                <v-text-field
		                    v-model="email"
		                    :rules="emailRules"
		                    label="E-mail"
		                    required
		                    append-icon="mdi-email"
		                >
		                </v-text-field>

		                <div class="text-xs-center">
		                    <v-btn
		                        color="success lighten-1"
		                        :disabled="!expire"
		                        @click="regenerate"
		                    >
		                        Regenerate
		                        <v-icon right dark>mdi-lock-open</v-icon>
		                    </v-btn>
		                </div>
		            </v-form>
		        </v-container>
		    </v-card>

		    <div>
				<b-alert
				  :show="dismissCountDown"
				  dismissible
				  :variant="alertVar"
				  @dismissed="dismissCountDown=0"
				  @dismiss-count-down="countDownChanged"
				>
					@{{alertMessage}}
				</b-alert>
			</div>

		  	<v-card>
		    	<v-footer>
		    		<v-card-text class="text-center">
		    			2020 - <strong>GmiDonasiApp</strong>
		    		</v-card-text>
		    	</v-footer>
		  	</v-card>
		</v-app>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0/dist/vue.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
	<script src="https://unpkg.com/vuex@2.0.0/dist/vuex.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js"></script>

	<script type="module">
		Vue.use(Vuetify)
		Vue.use(BootstrapVue)

		var app = new Vue({
			el: '#verif',
			vuetify: new Vuetify(),
			
			data(){
	            return {
	                valid: true,
	                expire: true,
	                verify: true,
	                otp: '',
					otpRules:[
	                    v=> !!v||'OTP required',
	                    v=> (!isNaN(parseFloat(v)) && v >= 100000 && v <= 999999) || 'OTP has to be 6 digit number'
	                ],
	                dismissSecs: 6,
        			dismissCountDown: 0,
        			alertMessage: 'aaaaa',
        			alertVar: '',
        			expired: false,
        			email: '',
	                emailRules: [
	                    v=> !!v||'Email required',
	                    v=> /([a-zA-Z0-9_]{1,})(@)([a-zA-Z0-9_]{2,})+/.test(v) || 'Email must be valid'
	                ],
	                password: '',
	                confirmPassword: '',
	                passwordRules:[
	                    v=> !!v||'Password required',
	                    v=> (v && v.length >= 8) || 'Min 8 characters'
	                ],
	                verified: false,
	                emailVerified: '',
	            }
	        },
	        methods: {
	        	goHome(){
	        		setTimeout(function(){
	        			window.location="/"
	        		}, 3000)
	        	},
	        	countDownChanged(dismissCountDown) {
					this.dismissCountDown = dismissCountDown
				},
				showAlert() {
					this.dismissCountDown = this.dismissSecs
				},
	            submit(){
	                if(this.$refs.form.validate()){
	                    let formData = {
	                        'otp'  : this.otp
	                    }

	                    axios.post('api/auth/verification', formData)
	                    .then((response)=>{
	                        let data = response.data

	                        if(data.response_code == '00'){
	                        	this.alertMessage = 'Verification success'
	                        	this.alertVar = 'success'
	                        	this.verified = true
	                        	this.valid = false
	                        	this.emailVerified = data.data.email
	                        	// console.log(data)
	                            this.showAlert()
	                        } else {
	                        	this.alertMessage = 'Verification failed. \n'+data.response_message
	                        	this.alertVar = 'danger'
	                            this.showAlert()
	                            if(data.response_code == '02'){
	                            	this.expired = true
	                            }
	                            // console.log(data.response_message)
	                        }
	                    })
	                    .catch((error)=>{
	                        let responses = error.response
	                        console.log(responses)
	                        this.alertMessage = 'Verification failed. \n'+responses.data.message
	                        this.alertVar = 'danger'
	                        this.showAlert()
	                        // console.log('gagal')
	                    })

						this.alertMessage = ''
						this.alertVar = ''
	                }
	            },
	            createPassword(){
	                if(this.$refs.newpass.validate()){
	                    let formData = {
	                        'email'  : this.emailVerified,
	                        'password'  : this.password,
	                        'password_confirmation'  : this.confirmPassword
	                    }

	                    axios.post('api/auth/update-password', formData)
	                    .then((response)=>{
	                        let data = response.data

	                        if(data.response_code == '00'){
	                        	this.alertMessage = 'Update Password success'
	                        	this.alertVar = 'success'
	                        	this.verified = false
	                        	this.valid = true
	                        	this.password = ''
	                			this.confirmPassword = ''
	                			this.goHome()
	                            this.showAlert()
	                        } else {
	                        	this.alertMessage = 'Update Password failed. \n'+data.response_message
	                        	this.alertVar = 'danger'
	                            this.showAlert()
	                            // console.log(data.response_message)
	                        }
	                    })
	                    .catch((error)=>{
	                        let responses = error.response
	                        console.log(responses)
	                        this.alertMessage = 'Update Password failed. \n'+responses.data.message
	                        this.alertVar = 'danger'
	                        this.showAlert()
	                        // console.log('gagal')
	                    })

						this.alertMessage = ''
						this.alertVar = ''
	                }
	            },
	            regenerate(){
	                if(this.$refs.regenform.validate()){
	                    let formData = {
	                        'email'  : this.email
	                    }

	                    axios.post('api/auth/regenerate-otp', formData)
	                    .then((response)=>{
	                        let data = response.data

	                        if(data.response_code == '00'){
	                        	this.alertMessage = 'Regenerate OTP success. Please check your email'
	                        	this.alertVar = 'success'
	                        	this.expired = false
	                        	this.email = ''
	                            this.showAlert()
	                        } else {
	                        	this.alertMessage = 'Regenerate OTP failed. \n'+data.response_message
	                        	this.alertVar = 'danger'
	                            this.showAlert()
	                            // console.log(data.response_message)
	                        }
	                    })
	                    .catch((error)=>{
	                        let responses = error.response
	                        // this.alertMessage = responses.data.response_message
	                        this.alertMessage = 'Regenerate OTP failed. \n'+data.response_message
	                        this.alertVar = 'danger'
	                        this.showAlert()
	                        // console.log('gagal')
	                    })

						this.alertMessage = ''
						this.alertVar = ''
	                }
	            },
	        },
		})
	</script>

</body>
</html>