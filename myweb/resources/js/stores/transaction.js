export default {
  namespaced : true,
  state: {
    transactions: 0,
  },
  mutations: {
    insert:  (state, payload) =>{
    	state.transactions++
    },
    reset: (state,value) => {
      state.transactions = value
    },
  },
  actions: {
    reset: ({commit}) => {
      commit('reset',0)
    },
  },
  getters: {
  	transactions : state => state.transactions
  }
}