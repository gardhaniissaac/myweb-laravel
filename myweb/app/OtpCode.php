<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\UuidTrait;

class OtpCode extends Model
{
    protected $table = 'otp_codes';

    use UuidTrait;

    public $guarded = [];

    protected $hidden = ['user_id', 'id', 'created_at', 'updated_at'];
    
    public function getUser(){
    	return $this->belongTo('App\User','user_id');
    }
}
