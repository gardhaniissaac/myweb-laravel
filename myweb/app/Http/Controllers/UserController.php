<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function detail(Request $request)
    {
        if(!$user = $request->user()){
            return 'Anda belum login';
        }
        
        return response()->json([
            "response_code" => "00",
            "reponse_message" => "Profile berhasil ditemukan",
            "data" => compact('user')
        ], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), 
            [ 
              'photo'  => ['nullable','mimes:png,jpg,jpeg','max:2048'],
            ]);   

        $user = $request->user();

        if ($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }  
        if ($name = request('name')) {
            $user->name = $name;
        }
      
        if ($files = $request->file('photo')) {
                 
            //store file into document folder
            $path = $request->file('photo')->store('/images/profile');
            $extension = $files->extension();
            if($user->photo){
                $hapus = Storage::delete($user->photo);
            }
            $new_path = Storage::move($path, '/images/profile/'.$user->id.'.'.$extension);
     
            //store your file into database
            $user->photo = '/images/profile/'.$user->id.'.'.$extension;
        }

        $user->save();
        
        return response()->json([
            "response_code" => "00",
            "reponse_message" => "Profile berhasil diperbarui",
            "data" => compact('user')
        ], 200);
    }
}
