<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    public function random($count){
    	$blog = Blog::select('*')
    				->inRandomOrder()
    				->limit($count)
    				->get();

    	$data['blogs'] = $blog;

    	return response()->json([
    		'response_code' => '00',
    		'response_message' => 'data blog berhasil ditambahkan',
    		'data'		=> $data

    	], 200);
    }

    public function index(){
        $blog = Blog::paginate(2);

        $data['blogs'] = $blog;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data blog berhasil ditampilkan',
            'data'      => $data

        ], 200);
    }

    public function detail($id){
        $blog = Blog::find($id);

        $data['blog'] = $blog;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data blog berhasil ditampilkan',
            'data'      => $data

        ], 200);
    }

    public function store(Request $request){
    	$request->validate([
    		'title' => 'required',
    		'description' => 'required',
            'author' => 'required',
            'image'     => 'required|mimes:jpg,jpeg,png'
    	]);

    	$blog = Blog::create([
    		'title' => $request['title'],
            'author' => $request['author'],
    		'description' => $request['description']
    	]);

        $data['blogs'] = $blog;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_extension = $image->extension();
            $image_name = $blog->id.".".$image_extension;
            $image_folder = '/photos/blog/';
            $image_location = $image_folder.$image_name;

            try {
                $image->move(public_path($image_folder), $image_name);
                $blog->update([
                    'image' => $image_location
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'photo blog gagal diunggah',
                    'data' => $data
                ], 200);
            }
        }

        $data['blogs'] = $blog;

    	return response()->json([
    		'response_code' => '00',
    		'response_message' => 'data blog berhasil ditambahkan',
    		'data' => $data
    	], 200);

    }
}
