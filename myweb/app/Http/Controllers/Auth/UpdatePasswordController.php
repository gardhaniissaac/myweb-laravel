<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UpdatePasswordRequest;
use Illuminate\Support\Facades\Hash;
use App\User;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(UpdatePasswordRequest $request)
    {
        request()->validate([
        ]);

        $user = User::where('email','=',request('email'))->first();

        if(!$user){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'User dengan email tersebut belum terdaftar'
            ]);
        }

        if($user->email_verified_at == null){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email belum diverifikasi'
            ]);
        }

        if(request('password') != request('password_confirmation')){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Konfirmasi password tidak sesuai'
            ]);
        }

        $user->password = Hash::make(request('password'));
        $user->save();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Password '.$user->name.' berhasil diperbarui'
        ]);
    }
}
