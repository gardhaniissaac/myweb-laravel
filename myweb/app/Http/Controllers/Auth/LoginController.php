<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only(['email','password']);

        if(!$token = auth()->attempt($credentials)){
            return response()->json([
                "error" => "Email dan Password tidak sesuai"
            ],400);
        }

        $user = User::where('email','=',request('email'))->first();

        if($user->email_verified_at == null){
            return response()->json([
                "response_code" => "00",
                "error" => "Email belum terverifikasi",
            ],400);
        }

        if($user->password == '8324hrfksdfhLKASEJFOIEWJ94jf29LKJASD99sjE9j4jD0sdv9wjw3fj'){
            return response()->json([
                "response_code" => "00",
                "error" => "Password akun belum didaftarkan. Silahkan lakukan update password.",
            ],400);
        }

        return response()->json([
            "response_code" => "00",
            "reponse_message" => "User berhasil login",
            "data" => [
                'token' => $token,
                'user' => $user
            ],
        ]);
    }
}
