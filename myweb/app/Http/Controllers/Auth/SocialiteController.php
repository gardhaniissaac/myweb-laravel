<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Carbon\Carbon;
use App\User;
use App\Role;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider){
    	$url = url(Socialite::driver($provider)->stateless()->redirect()->getTargetUrl());

    	return response()->json([
    		'url' => $url
    	]);
    }

    public function handleProviderCallback($provider){
    	try {
    		$social_user = Socialite::driver($provider)->stateless()->user();

    		// dd($social_user);

    		if(!$social_user){
    			return response()->json([
	    			'response_code' => '01',
	    			'response_message' => 'login failed1'
	    		], 401);
    		}

    		$user = User::whereEmail($social_user->email)->first();
    		$role_user = Role::where('role_name','=','User')->first();

    		if(!$user){
    			if($provider == 'google'){
    				$photo = $social_user->avatar;
    			}
    			$user = User::create([
    				'email' => $social_user->email,
    				'name'	=> $social_user->name,
    				'role_id' => $role_user->id,
    				'email_verified_at'	=> Carbon::now(),
    				'photo'	=> $photo
    			]);
    		}

    		$data['user'] = $user;
    		$data['token'] = auth()->login($user);

    		return response()->json([
    			'response_code' => '00',
    			'response_message' => 'login success',
    			'data' => $data
    		], 200);
    	} catch (\Throwable $th){
    		return response()->json([
    			'response_code' => '01',
    			'response_message' => 'login failed2'
    		], 401);
    	}
    }
}
