<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\VerificationRequest;
use App\OtpCode;
use App\User;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(VerificationRequest $request)
    {
        request()->validate([
        ]);

        $otp = OtpCode::where('otp_code','=',$request->otp)->first();

        if(!$otp){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Kode OTP tidak ditemukan'
            ]);
        }

        $user = User::find($otp->user_id);

        if($user->email_verified_at != null){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email telah diverifikasi pada: '.$user->email_verified_at
            ]);
        }

        if(date("Y-m-d H:i:s")>$otp->valid_until){
            return response()->json([
                'response_code' => '02',
                'response_message' => 'Kode OTP tidak berlaku. Silahkan generate kode OTP yang baru'
            ]);
        }

        $user->email_verified_at = date("Y-m-d H:i:s");
        $user->save();

        $otp->delete();
        $email_user = $user->email;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Email '.$email_user.' telah diverifikasi.',
            'data'  => $user
        ]);
    }
}
