<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegenerateOtpRequest;
use App\OtpCode;
use App\User;
use App\Events\RegenerateOTPEvent;

class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegenerateOtpRequest $request)
    {
        request()->validate([
        ]);

        $user = User::where('email','=',request('email'))->first();
        
        if(!$user){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'User dengan email tersebut belum terdaftar'
            ]);
        }

        $otp = OtpCode::updateOrCreate(
            ['user_id'   => $user->id],
            [
                'otp_code' => rand(100000,999999),
                'valid_until' => date("Y-m-d H:i:s",strtotime("+5 min"))
        ]);

        event(new RegenerateOTPEvent($user,$otp));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Kode OTP telah diperbarui.'
        ]);
    }
}
