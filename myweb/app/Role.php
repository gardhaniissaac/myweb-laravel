<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\UuidTrait;

class Role extends Model
{
    use UuidTrait;

    protected $table = 'roles';

    public function getUser(){
    	return $this->hasMany('App\User','role_id');
    }
}
