<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\UuidTrait;

class Campaign extends Model
{
	protected $table = 'campaign';

    use UuidTrait;

    public $guarded = [];
}
