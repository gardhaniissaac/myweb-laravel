<?php

namespace App\Listeners;

use App\Events\RegenerateOTPEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\RegenerateOTPMail;

class SendEmailRegenerateOTP implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOTP  $event
     * @return void
     */
    public function handle(RegenerateOTPEvent $event)
    {
        Mail::to($event->user)->send(new RegenerateOTPMail($event->user,$event->otp));
    }
}
