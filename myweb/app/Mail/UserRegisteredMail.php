<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user,$otp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $otp)
    {
        $this->user = $user;
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
                ->view('mail.send_email_user_registered')
                ->with([
                        'name' => $this->user->name,
                        'otp' => $this->otp->otp_code,
                    ]);
    }
}
